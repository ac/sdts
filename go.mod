module sdts

go 1.23.1

require (
	catinello.eu/capabilities v0.0.0-20241111172555-e4ed37e59a5a
	catinello.eu/cli v0.0.0-20241222110728-1a267103e7c3
	catinello.eu/com v0.0.0-20241117145441-ff2de89e869d
	catinello.eu/restrict v0.0.0-20250213134751-c1d110c5d07c
	golang.org/x/net v0.35.0
)

require (
	catinello.eu/inet v0.0.0-20241127115856-0e99a8ad6f71 // indirect
	catinello.eu/store v0.0.0-20250213134558-efb16c19f7d1 // indirect
	github.com/seccomp/libseccomp-golang v0.10.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/text v0.22.0 // indirect
	kernel.org/pub/linux/libs/security/libcap/cap v1.2.73 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.73 // indirect
)
