// SPDX-FileCopyrightText: 2021 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build unix

package main

import (
	"os"

	"catinello.eu/cli"
	"catinello.eu/com"
)

func direct(x cli.Data) error {
	c := x.Pass.(*com.Config)

	switch x.Name() {
	case "help":
		help(c)
		os.Exit(0)
	case "license":
		c.Print(license)
		os.Exit(0)
	case "version":
		c.Println(version)
		os.Exit(0)
	}

	return nil
}

func help(c *com.Config) {
	c.Println("sdts - Simple date time setter.")
	c.Println()
	c.Println("Description:")
	c.Println("  Set date and time on a Linux/BSD system, using data from an NTP or HTTP server.")
	c.Println()
	c.Println("Usage:")
	c.Println("  sdts [OPTIONS] NTP-/WEB-ADDRESS")
	c.Println()
	c.Println("Examples:")
	c.Println("  sdts 2.pool.ntp.org")
	c.Println("  sdts http://time.is")
	c.Println("  sdts -d -s -w https://one.one.one.one")
	c.Println()
	c.Println("Options:")
	c.Println("  -d | --dryrun  | Does not change date/time.")
	c.Println("  -s | --silent  | No output.")
	c.Println()
	c.Println("  Proxy connections only for web requests.")
	c.Println("  -x | --socks   | Use SOCKS proxy config from environment.")
	c.Println("  -w | --http    | Use HTTP proxy config from environment.")
	c.Println()
	c.Println("  --help         | Show this help.")
	c.Println("  --license      | Print license.")
	c.Println("  --version      | Print version.")
	c.Println()
	c.Println("Environment variables:")
	c.Println("  TIME_DIFF      | Threshold in Milliseconds (Default: 500)")
	c.Println("  NTP_PORT       | Set destination port value. (Default: 123)")
	c.Println("  ALL_PROXY      | Use given SOCKS proxy for web requests. socks5(h)://user:pass@host:port")
	c.Println("  HTTP(S)_PROXY  | Use given HTTP proxy for web requests. http(s)://user:pass@host:port")
	c.Println()
	c.Println("Website:")
	c.Println("  https://catinello.eu/sdts")
	c.Println()
	c.Println("License:")
	c.Println("  BSD-3-Clause License © 2021 Antonino Catinello")
	c.Println()
	c.Println("Version:")
	c.Println("  " + version)
}
