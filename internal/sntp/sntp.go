// SPDX-FileCopyrightText: 2021 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

// Package sntp retrieves the current time from an NTP server.
package sntp

import (
	"fmt"
	"net"
	"strings"
	"time"
)

func isIPv4(s string) bool {
	ip := net.ParseIP(s)
	if ip != nil {
		t := ip.To4()
		if t != nil {
			return true
		}
	}

	return false
}

func isIPv6(s string) bool {
	if len(s) >= net.IPv6len {
		ip := net.ParseIP(s)
		if ip != nil {
			return true
		}
	}

	return false
}

// Resolve takes a DNS entry as string and returns resolved addresses in
// slices for v4 and v6 addresses along with nil or an encountered error.
func resolve(name string) ([]net.IP, []net.IP, error) {
	ips, err := net.LookupHost(name)
	if err != nil {
		return nil, nil, err
	}

	var v4, v6 []net.IP

	for _, v := range ips {
		if len(v) >= net.IPv6len {
			ip := net.ParseIP(v)
			if ip != nil {
				v6 = append(v6, ip)
				continue
			}
		}

		ip := net.ParseIP(v)
		if ip != nil {
			t := ip.To4()
			if t != nil {
				v4 = append(v4, ip)
				continue
			}
		}
	}

	return v4, v6, nil
}

// Dial wraps the DialUDP function and differentiats by IPv4/IPv6.
func dial(v6 bool, ip net.IP, port string) (*net.UDPConn, error) {
	var err error
	var addr *net.UDPAddr

	if v6 {
		addr, err = net.ResolveUDPAddr("udp6", fmt.Sprintf("[%s]:%s", ip.String(), port))
	} else {
		addr, err = net.ResolveUDPAddr("udp4", fmt.Sprintf("%s:%s", ip.String(), port))
	}

	if err != nil {
		return nil, err
	}

	if v6 {
		return net.DialUDP("udp6", nil, addr)
	} else {
		return net.DialUDP("udp4", nil, addr)
	}

}

// Now takes a remote NTP server name (DNS).
// It returns a time.Time type of the current time in UTC with nil along or an error.
func Now(server, port string) (*time.Time, error) {
	var conn *net.UDPConn
	var err error

	/*
		server, port, err := net.SplitHostPort(hostport string) (host, port string, err error)
		if err != nil {
			return nil, err
		}
	*/
	if isIPv6(server) {
		conn, err = dial(true, net.ParseIP(server), port)
		if err != nil {
			return nil, err
		}
	} else if isIPv4(server) {
		conn, err = dial(false, net.ParseIP(server), port)
		if err != nil {
			return nil, err
		}
	} else {
		v4, v6, err := resolve(server)
		if err != nil {
			return nil, err
		}

		var v6call bool

		for n, ip := range v6 {
			conn, err = dial(true, ip, port)
			if err != nil {
				if len(v6) > n+1 {
					continue
				} else {
					if strings.Contains(err.Error(), "network is unreachable") && len(v4) > 0 {
						break
					} else {
						return nil, err
					}
				}
			}

			v6call = true
		}

		if v6call == false {
			for n, ip := range v4 {
				conn, err = dial(false, ip, port)
				if err != nil {
					if len(v4) > n+1 {
						continue
					} else {
						return nil, err
					}
				}
			}
		}

	}

	defer conn.Close()
	conn.SetDeadline(time.Now().Add(10 * time.Second))

	// the following is largly based on https://play.golang.org/p/6KRE-2Hq6n Thanks to whoever wrote that.
	packet := make([]byte, 48)
	packet[0] = 0x1B

	_, err = conn.Write(packet)
	if err != nil {
		return nil, err
	}

	_, err = conn.Read(packet)
	if err != nil {
		return nil, err
	}

	var second, fraction uint64

	second = uint64(packet[40])<<24 | uint64(packet[41])<<16 | uint64(packet[42])<<8 | uint64(packet[43])
	fraction = uint64(packet[44])<<24 | uint64(packet[45])<<16 | uint64(packet[46])<<8 | uint64(packet[47])

	nsec := (second * 1e9) + ((fraction * 1e9) >> 32)
	now := time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC).Add(time.Duration(nsec))

	return &now, nil
}
