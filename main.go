// SPDX-FileCopyrightText: 2021 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build unix

package main

import (
	_ "embed"

	"context"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"syscall"
	"time"

	"catinello.eu/capabilities"
	"catinello.eu/cli"
	"catinello.eu/com"
	"catinello.eu/restrict"

	"golang.org/x/net/http/httpproxy"
	"golang.org/x/net/proxy"

	"sdts/internal/sntp"
)

//go:embed LICENSE
var license string

var version string

// Threshold is a time.Duration value multiplied with time.Milliseconds
const THRESHOLD = time.Duration(500)
const NTPPORT string = "123"

type state struct {
	m         *com.Config
	socks     bool
	http      bool
	ntpport   string
	threshold time.Duration
	silent    bool
	dryrun    bool
	target    string
}

func main() {
	var s state
	var dialer proxy.Dialer
	var err error
	var t *time.Time
	var code string

	opts := cli.New()

	if len(os.Getenv("DEBUG")) > 0 {
		s.m = com.New(com.Debug)
		opts.Debug(s.m.Logger(com.Common, "cli: ", false))
	} else {
		s.m = com.New(com.Common)
	}

	s.m.D().Println("DEBUG: ", os.Getenv("DEBUG"))
	s.m.D().Println("ARGS: ", os.Args)

	if err := restrict.Syscalls("capabilities stdio rpath dns inet settime unveil"); err != nil {
		s.m.E().Println(err)
		os.Exit(255)
	}

	if err := restrict.AccessCerts(); err != nil {
		s.m.E().Println(err)
		os.Exit(255)
	}

	if err := restrict.AccessLock(); err != nil {
		s.m.E().Println(err)
		os.Exit(255)
	}

	if len(os.Getenv("TIME_DIFF")) > 0 {
		ms, err := strconv.Atoi(os.Getenv("TIME_DIFF"))
		if err != nil {
			s.m.E().Println("✗ | " + err.Error())
			os.Exit(3)
		}

		s.threshold = time.Duration(ms)
	} else {
		s.threshold = THRESHOLD
	}

	if len(os.Getenv("NTP_PORT")) > 0 {
		s.ntpport = os.Getenv("NTP_PORT")
	} else {
		s.ntpport = NTPPORT
	}

	opts.Add(cli.Bool, 0, "version").Pass(cli.Data{Pass: s.m}).Direct(direct)
	opts.Add(cli.Bool, 0, "license").Pass(cli.Data{Pass: s.m}).Direct(direct)
	opts.Add(cli.Bool, 0, "help").Pass(cli.Data{Pass: s.m}).Direct(direct)
	opts.Add(cli.Bool, 'd', "dryrun")
	opts.Add(cli.Bool, 's', "silent")
	opts.Add(cli.Bool, 'x', "socks")
	opts.Add(cli.Bool, 'w', "http")

	if err := opts.Args(); err != nil {
		s.m.E().Println(err)
		help(s.m)
		os.Exit(1)
	}

	targets, r := opts.Filtered()
	if len(r) > 0 {
		s.m.Fatal(1, "Invalid options called: ", r)
	}

	if len(targets) != 1 {
		help(s.m)
		os.Exit(1)
	} else {
		s.target = targets[0]
	}

	s.dryrun = opts.Bool("dryrun")
	s.silent = opts.Bool("silent")
	s.socks = opts.Bool("socks")
	s.http = opts.Bool("http")

	if err := capabilities.Set("cap_sys_time=p"); err != nil {
		s.m.E().Println(err)
		os.Exit(254)
	}

	s.m.D().Println("HTTP_PROXY: ", s.http)
	s.m.D().Println("ALL_PROXY (socks): ", s.socks)

	if strings.HasPrefix(s.target, "http") {
		if s.http {
			httpproxy := httpproxy.FromEnvironment()
			s.m.D().Println(httpproxy)
			var prx string

			if len(httpproxy.HTTPSProxy) > 0 {
				prx = httpproxy.HTTPSProxy
			} else {
				prx = httpproxy.HTTPProxy
			}

			if len(prx) == 0 {
				s.m.E().Println("HTTP-proxy environment variables are not set.")
				os.Exit(2)
			}

			url, err := url.Parse(prx)
			if err != nil {
				s.m.E().Println(err)
				os.Exit(2)
			}

			transport := &http.Transport{Proxy: http.ProxyURL(url), DisableKeepAlives: true, Dial: (&net.Dialer{
				Timeout:   10 * time.Second,
				KeepAlive: 10 * time.Second,
			}).Dial,
				TLSHandshakeTimeout:   5 * time.Second,
				ResponseHeaderTimeout: 5 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			}

			client := &http.Client{
				Transport: transport,
			}

			t, err, code = get(s.target, client)
			s.m.D().Println("HTTP: " + code)
			if code == "HTTP ERROR" {
				s.m.E().Println("✗ | Web request over http-proxy failed.")
				os.Exit(3)
			}
		} else if s.socks {
			dialer = proxy.FromEnvironment()
			s.m.D().Println(dialer)

			if len(os.Getenv("ALL_PROXY")) == 0 && len(os.Getenv("all_proxy")) == 0 {
				s.m.E().Println("SOCKS-proxy environment variables are not set.")
				os.Exit(2)
			}

			for _, v := range []string{"all_proxy", "ALL_PROXY"} {
				if len(os.Getenv(v)) > 0 {
					prx := os.Getenv(v)
					if !strings.HasPrefix(prx, "socks5") {
						s.m.E().Println("SOCKS-proxy environment variable has wrong syntax.")
						os.Exit(2)
					}
				}
			}

			dialctx := func(ctx context.Context, network, address string) (net.Conn, error) {
				return dialer.Dial(network, address)
			}

			transport := &http.Transport{DialContext: dialctx, DisableKeepAlives: true, Dial: (&net.Dialer{
				Timeout:   10 * time.Second,
				KeepAlive: 10 * time.Second,
			}).Dial,
				TLSHandshakeTimeout:   5 * time.Second,
				ResponseHeaderTimeout: 5 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			}

			client := &http.Client{Transport: transport}
			t, err, code = get(s.target, client)
			s.m.D().Println("HTTP: " + code)
			if code == "HTTP ERROR" {
				s.m.E().Println("✗ | Web request over socks-proxy failed.")
				os.Exit(3)
			}
		} else {
			t, err, code = get(s.target, http.DefaultClient)
			s.m.D().Println("HTTP: " + code)
			if code == "HTTP ERROR" || err != nil {
				s.m.E().Println("✗ | Web request failed. (HTTP code: " + code + ") " + err.Error())
				os.Exit(3)
			}
		}
	} else {
		if s.socks == true || s.http == true {
			s.m.E().Println("Only web requests over proxy allowed.")
			os.Exit(2)
		}
		t, err = sntp.Now(s.target, s.ntpport)
	}

	if s.dryrun {
		if err := restrict.Syscalls("stdio"); err != nil {
			s.m.E().Println(err)
			os.Exit(255)
		}
	} else {
		if err := restrict.Syscalls("stdio settime"); err != nil {
			s.m.E().Println(err)
			os.Exit(255)
		}
	}

	x, err := s.eval(t, err)
	if err != nil {
		if x == 3 {
			s.m.E().Println("✗ | " + err.Error())
		} else {
			s.m.E().Println(err)
		}
	}

	os.Exit(x)
}

func get(server string, client *http.Client) (*time.Time, error, string) {
	resp, err := client.Get(server)
	if err != nil {
		return &time.Time{}, err, "HTTP ERROR"
	}

	date, err := time.Parse(time.RFC1123, resp.Header.Get("Date"))
	return &date, err, resp.Status
}

func (s *state) eval(t *time.Time, err error) (int, error) {
	var code int

	if err != nil {
		return 3, err
	}

	l := time.Now()

	d := t.Sub(l)

	if d < s.threshold*time.Millisecond && d > -s.threshold*time.Millisecond {
		if s.silent == false {
			s.m.Println("Δ " + d.String() + " ✓ | NO CHANGE")
		}
		code = 0
		return code, nil
	} else {
		if s.silent == false {
			s.m.Print("Δ " + d.String() + " ✗ | ")
		}
		code = 1
	}

	if s.dryrun == false {
		err = set(t)
		if err != nil {
			if s.silent == false {
				s.m.Println("FAILED")
			}
			return 2, err
		}
	}

	if s.silent == false {
		s.m.Println("CHANGED")
	}

	return code, nil
}

// set needs CAP_SYS_TIME on linux to run as user
// setcap cap_sys_time+p sdts
func set(t *time.Time) error {
	tv := syscall.NsecToTimeval(t.UnixNano())
	return syscall.Settimeofday(&tv)
}
